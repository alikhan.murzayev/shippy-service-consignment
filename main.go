package main

import (
	"context"
	"log"
	"sync"

	"github.com/micro/go-micro"

	pb "gitlab.com/alikhan.murzayev/shippy-service-consignment/proto/consignment"
)

const (
	port = ":50051"
)

// ------------------------ Repository ------------------------ //

type Repository interface {
	Create(*pb.Consignment) (*pb.Consignment, error)
	GetAll() ([]*pb.Consignment, error)
}

type repository struct {
	sync.RWMutex
	consignments []*pb.Consignment
}

func (repo *repository) Create(consignment *pb.Consignment) (*pb.Consignment, error) {

	repo.RWMutex.Lock()
	defer repo.RWMutex.Unlock()

	updated := append(repo.consignments, consignment)
	repo.consignments = updated

	return consignment, nil
}

func (repo *repository) GetAll() ([]*pb.Consignment, error) {
	return repo.consignments, nil
}

func NewRepository() Repository {
	return &repository{}
}

// -------------------- Service --------------------- //

type Service interface {
	CreateConsignment(context.Context, *pb.Consignment, *pb.Response) error
	GetConsignments(context.Context, *pb.GetRequest, *pb.Response) error
}

type service struct {
	repo Repository
}

func (s *service) CreateConsignment(ctx context.Context, req *pb.Consignment, resp *pb.Response) error {

	consignment, err := s.repo.Create(req)
	if err != nil {
		return err
	}

	resp.Created = true
	resp.Consignment = consignment

	return nil
}

func (s *service) GetConsignments(ctx context.Context, req *pb.GetRequest, resp *pb.Response) error {
	consignments, err := s.repo.GetAll()
	if err != nil {
		return err
	}

	resp.Consignments = consignments

	return nil
}

func NewService(repo Repository) Service {
	return &service{repo: repo}
}

func main() {

	repo := NewRepository()
	service := NewService(repo)

	srv := micro.NewService(
		micro.Name("shippy.service.consignment"),
	)
	srv.Init()

	pb.RegisterShippingServiceHandler(srv.Server(), service)

	if err := srv.Run(); err != nil {
		log.Fatalf("failed to run: %v", err)
	}

}
